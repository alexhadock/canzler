import yaml

def check_structure(file_path, expected_structure):
    with open(file_path, 'r') as file:
        data = yaml.safe_load(file)
        if data != expected_structure:
            raise ValueError(f"Структура файла {file_path} не соответствует ожидаемой")

def read_yaml_file(file_path):
    with open(file_path, 'r') as file:
        return yaml.safe_load(file)

