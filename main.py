import asyncio
from db_operations import PostgresDB, MongoDB
from file_operations import check_structure, read_yaml_file

async def process_users(users_data, databases_data):
    for host, user_info in users_data.items():
        if host in databases_data:
            db_info = databases_data[host]
            for username, password in user_info.items():
                if not await db_info['db'].check_user_existence(username):
                    await db_info['db'].create_user(username)

async def process_grants(grants_data, users_data, databases_data):
    for user, host_data in grants_data.items():
        if user in users_data:
            for host, db_data in host_data.items():
                if host in databases_data:
                    for db, scheme_data in db_data.items():
                        for scheme, table_data in scheme_data.items():
                            table_name, permissions = list(table_data.items())[0]
                            # Выдача прав пользователю в БД
                            # Реализация выдачи прав

async def main():
    # Проверка наличия файлов/директорий
    # ...

    # Проверка структуры файлов
    # ...

    # Чтение данных из файлов
    databases_data = read_yaml_file('databases.yaml')
    users_data = read_yaml_file('users.yaml')
    grants_data = read_yaml_file('grants.yaml')

    # Инициализация подключений к БД
    for host, info in databases_data.items():
        if info['type'] == 'postgres':
            db = PostgresDB(info['host'], info['port'], info['user'], info['password'])
        elif info['type'] == 'mongo':
            db = MongoDB(info['host'], info['port'])
        databases_data[host]['db'] = db

    # Обработка пользователей из users.yaml
    await process_users(users_data, databases_data)

    # Выдача прав из grants.yaml
    await process_grants(grants_data, users_data, databases_data)

if __name__ == "__main__":
    asyncio.run(main())
