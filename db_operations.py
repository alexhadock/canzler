import asyncio
import asyncpg
from motor.motor_asyncio import AsyncIOMotorClient

class PostgresDB:
    def __init__(self, host, port, user, password):
        self.host = host
        self.port = port
        self.user = user
        self.password = password

    async def check_user_existence(self, username):
        async with asyncpg.connect(user=self.user, password=self.password, database="postgres") as conn:
            query = f"SELECT usename FROM pg_user WHERE usename = '{username}'"
            result = await conn.fetch(query)
            return bool(result)

    async def create_user(self, username):
        async with asyncpg.connect(user=self.user, password=self.password, database="postgres") as conn:
            query = f"CREATE USER {username} WITH PASSWORD 'password'"
            await conn.execute(query)

class MongoDB:
    def __init__(self, host, port):
        self.client = AsyncIOMotorClient(host, port)

    async def check_user_existence(self, username):
        db = self.client["admin"]
        result = await db.command("usersInfo", username)
        return bool(result)

    async def create_user(self, username):
        db = self.client["admin"]
        await db.command("createUser", username, pwd="password")

